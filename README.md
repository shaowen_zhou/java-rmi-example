# Java-RMI-Example
A simple example of a Java-RMI setup using Maven. Reference: https://github.com/rm5248/Java-RMI-Example .

## Project Setup

There are three subprojects in this example: RMIClient, RMIServer, and RMIInterfaces.  These should be self-explanatory.  The server sets up a basic class to be used as a remote object.  The client then connects to it.  Since both the client and the server need to be able to know the definition of the interface, that is broken out into a separate project that they both have as a dependency.

This project should build out of the box with Maven.

## Running the project

1. Use bash in Windows, no powershell
2. Maven dependency https://blog.csdn.net/gywtzh0889/article/details/52021615 
3. Go to interface target/classes folder and run ``rmiregistry``, otherwise rmiregistry will not find the desired class and throws ClassNotFoundException as RemoteException
4. Go to server target folder and run jar file with security manager<br>
e.g. ``java -jar Server.jar`` 
5. Go to client target folder and run jar file with security manager<br>
e.g. ``java -Djava.security.policy=client-policy.txt -jar Client.jar``
